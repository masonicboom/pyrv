'''Copyright (c) 2010 mason simon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

author: mason simon (masonsimon@gmail.com)
'''

import rv
import random
import collections


class Autoregressive(rv.RV):
  '''see http://en.wikipedia.org/wiki/Autoregressive_model
  '''

  def __init__(self, coefficients, Disturbance):
    '''coefficients is a list in which the i-th element is the coefficient on
    the i-th most recent realization of this RV.

    Disturbance is an RV defining the random component of this RV, which isn't
    dependent on the RV's previous realizations.
    '''
    self.coef = coefficients
    self.Disturbance = Disturbance

    # the i-th item in this deque holds this RV's i-th most recent realization
    order = len(coefficients)
    self.past = collections.deque([0]*order)

  def sample(self):
    def dot(X, Y):
      '''see http://en.wikipedia.org/wiki/Dot_product'''
      return sum([x*y for x,y in zip(X,Y)])

    x = dot(self.coef, self.past) + ~self.Disturbance

    self.past.appendleft(x)  # appending left makes self.past[0] = x
    self.past.pop()

    return x


if __name__ == '__main__':
  # this isn't such a hot test. it prints output in a format that R can easily
  # handle, so you copy stdout from here into R and then plot it to check if
  # that data looks autocorrelated
  N = 200
  import gaussian
  D = gaussian.Gaussian(mu=0, sigma=1)
  X = Autoregressive([1], D) + 4
  data = []
  for i in range(N):
    data.append(~X)
  print 'c(%s)' % ',\n'.join([str(x) for x in data])

