'''Copyright (c) 2010 mason simon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

author: mason simon (masonsimon@gmail.com)
'''

import rv
import random


class Gaussian(rv.RV):
  def __init__(self, mu=0, sigma=1):
    '''mu is the mean of this RV; sigma is the standard deviation.
    '''
    self.mu = mu
    self.sigma = sigma

  def sample(self):
    return random.gauss(self.mu, self.sigma)


# run some tests
if __name__ == '__main__':
  import math

  N = 10000
  
  mu = 0
  sigma = 1
  X = Gaussian(mu, sigma)
  
  # compute the mean and sd of the empirical distribution (observed)
  # this algorithm is based on http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#On-line_algorithm
  mean, M2 = 0, 0
  for i in xrange(1,N):
    o = ~X
    delta = o - mean
    mean = mean + delta/i
    M2 = M2 + delta*(o - mean)  # This expression uses the new value of mean
  sd = math.sqrt(M2/(i - 1))

  print 'mean: %f\tsd: %f' % (mean, sd)
