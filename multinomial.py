'''Copyright (c) 2010 mason simon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

author: mason simon (masonsimon@gmail.com)
'''

import rv
import random


class Multinomial(rv.RV):
  '''
  '''

  def __init__(self, distribution):
    '''distribution is a dictionary mapping between values in the domain and the
       probability mass on that value.

       if the probability masses are not normalized, they will be normalized

       if distribution is a list, rather than a dictionary, then a uniform
       distribution will be placed on each element in the list
    '''
    if isinstance(distribution, list):
      d = {}
      N = len(distribution)
      for k in distribution:
	d[k] = 1.0/N
      distribution = d
    elif isinstance(distribution, dict):
      pass # good--this is what we wanted
    else:
      raise TypeError('unsupported distribution type: %s' % type(distribution) )
    
    p_sum = 0.0
    for k in distribution:
      p = distribution[k]
      assert p >= 0
      p_sum += p

    # normalize distribution
    for k in distribution:
      distribution[k] /= p_sum

    self.distribution = distribution

  def sample(self):
    p = random.uniform(0.0, 1.0)
    p_sum = 0.0
    for (x, q) in self.distribution.iteritems():
      p_sum += q
      if p_sum > p: break

    if isinstance(x, rv.RV):
      return ~x

    return x


# run some tests
if __name__ == '__main__':
  # test that Multinomial sampling is correct by giving the desired
  # distribution and then testing the null hypothesis that it differs from the
  # empirical distribution, by using a chi-squared test.
  N = 10000
  
  drv = Multinomial( {'a':2, 'b':1, 'c':1} )
  expected = drv.distribution
  DOF = len(expected)-1
  
  # compute the empirical distribution (observed)
  observed = {'a':0, 'b':0, 'c':0}
  for i in xrange(0,N):
      observed[ drv.sample() ] += 1

  # normalize
  for key in observed:
      observed[key] /= float(N)

  chi_squared = 0.0
  for key in observed:
      o = observed[key]
      e = expected[key]
      chi_squared += (o-e)**2 / e
  print 'observed: %s' % observed
  print 'expected: %s' % expected
  print 'chi-squared test statistic with %d degrees of freedom and N=%d: %f' % (DOF, N, chi_squared)
  # see http://www.itl.nist.gov/div898/handbook/eda/section3/eda3674.htm
  assert chi_squared < 0.002
  print 'the observed and expected distributions are the same with high probability'
