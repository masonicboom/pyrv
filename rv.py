'''Copyright (c) 2010 mason simon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

author: mason simon (masonsimon@gmail.com)

This file defines Random Variables (RVs), which are different from normal
variables in the following ways:
1) RVs are read-only,
2) RVs are read by calling X.sample(), or more succinctly ~X, for the RV X,
3) the value you get by calling X.sample() will depend on the set of possible
   values for X, and the probability distribution it places on that set.
'''

NUMERIC = (int, float)


class RV(object):
  def __init__(self, sampling_function=None):
    '''you may optionally pass in a function that will be called to sample from
    this RV, which you might find useful if you're trying to encode an RV that's
    dependent on other RVs
    '''
    if sampling_function:
      self.sample = sampling_function

  def sample(self):
    raise NotImplementedError()
  
  def __invert__(self):
    '''this overload is just for the syntactic sugar to make ~X sample from X
    '''
    return self.sample()

  def __add__(self, other):
    if isinstance(other, NUMERIC) == False:
	raise NotImplemented
    return RV(sampling_function = lambda: ~self + other)

  def __radd__(self, other):
    return self.__add__(other)

  def __mul__(self, other):
    if isinstance(other, NUMERIC) == False:
	raise NotImplemented
    return RV(sampling_function = lambda: ~self * other)

  def __rmul__(self, other):
    return self.__mul__(other)

  def __sub__(self, other):
    return self + -1*other

  def __rsub__(self, other):
    return -1*self + other

